﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaintWpf
{
    /// <summary>
    /// Interaction logic for WidthPanel.xaml
    /// </summary>
    public partial class WidthPanel : UserControl
    {
        public static readonly DependencyProperty LineWidthProperty =
        DependencyProperty.Register("LineWidth", typeof(int), typeof(WidthPanel));

        public int LineWidth
        {
            get { return (int)GetValue(LineWidthProperty); }
            set { SetValue(LineWidthProperty, value); }
        }

        public WidthPanel()
        {
            InitializeComponent();
        }

        private void slWidth_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            LineWidth = (int)slWidth.Value;
        }
    }
}
