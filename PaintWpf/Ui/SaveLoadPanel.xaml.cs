﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaintWpf
{
    /// <summary>
    /// Interaction logic for SaveLoadPanel.xaml
    /// </summary>
    public partial class SaveLoadPanel : UserControl
    {
        public static readonly DependencyProperty TargetCanvasProperty =
        DependencyProperty.Register("TargetCanvas", typeof(Canvas), typeof(SaveLoadPanel));

        public Canvas TargetCanvas
        {
            get { return (Canvas)GetValue(TargetCanvasProperty); }
            set { SetValue(TargetCanvasProperty, value); }
        }

        public SaveLoadPanel()
        {
            InitializeComponent();
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = @"E:\";
            sfd.Filter = "bmp(.bmp)|*.bmp";
            if (sfd.ShowDialog() == true)
            {
                PictureBmp pb = new PictureBmp();
                pb.Save(sfd.FileName, TargetCanvas);
            }
        }
    }
}
