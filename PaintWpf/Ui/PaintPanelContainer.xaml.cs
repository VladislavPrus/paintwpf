﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.AvalonDock.Controls;

namespace PaintWpf
{
    /// <summary>
    /// Логика взаимодействия для PaintPanelContainer.xaml
    /// </summary>
    public partial class PaintPanelContainer : UserControl
    {
        public static readonly DependencyProperty PaintPanelProperty =
        DependencyProperty.Register("PaintData", typeof(PaintPanel), typeof(PaintPanelContainer));

        public PaintPanel PaintPanel
        {
            get { return (PaintPanel)GetValue(PaintPanelProperty); }
            set { SetValue(PaintPanelProperty, value); }
        }

        PaintPanel pp;

        public PaintPanelContainer()
        {
            InitializeComponent();
            AddNewPanel();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddNewPanel();
        }

        private void AddNewPanel()
        {
            LayoutDocument ld = new LayoutDocument();
            pp = new PaintPanel();
            PaintPanel = pp;
            ld.Content = pp;
            ld.Title = FindResource("ppc_Panel") + " " + (panel.Children.Count + 1);
            ld.IsActiveChanged += Ld_IsActiveChanged;
            //ld.IsSelectedChanged += Ld_IsSelectedChanged;
            panel.Children.Add(ld);
        }
        private void Ld_IsActiveChanged(object sender, EventArgs e)
        {
            var tab = sender as LayoutDocument;
            if(tab.IsActive)
            {
                //SetValue(PaintPanelProperty, tab.Content);
                PaintPanel = (PaintPanel)tab.Content;
            }          
            //a.FlowDirection = FlowDirection.LeftToRight;
            //cmd.pp = (PaintPanel)panel.SelectedContent.Content;
            //cmd.data = cmd.pp.data;
            //MessageBox.Show("asd");
        }
    }
}
