﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaintWpf
{
    /// <summary>
    /// Interaction logic for ColorPanel.xaml
    /// </summary>
    public partial class ColorPanel : UserControl
    {
        public static readonly DependencyProperty PaintDataColorProperty =
         DependencyProperty.Register("PaintDataColor", typeof(SolidColorBrush), typeof(ColorPanel));

        public SolidColorBrush PaintDataColor
        {
            get { return (SolidColorBrush)GetValue(PaintDataColorProperty); }
            set { SetValue(PaintDataColorProperty, value); }
        }

        public ColorPanel()
        {
            InitializeComponent();
        }

        private void ClrPcker_Background_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            PaintDataColor = PaintDataColor;
            PaintDataColor = new SolidColorBrush((Color)ClrPcker_Background.SelectedColor);          
        }
    }
}
