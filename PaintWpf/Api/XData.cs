﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PaintWpf
{
    public class XData
    {
        public SolidColorBrush Color { set; get; }
        public int Width { set; get; }

        public XData()
        {
            Color = new SolidColorBrush(Colors.Black);
            Width = 5;
        }
    }
}
