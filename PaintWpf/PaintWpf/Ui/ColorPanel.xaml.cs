﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaintWpf
{
    /// <summary>
    /// Interaction logic for ColorPanel.xaml
    /// </summary>
    public partial class ColorPanel : UserControl
    {
        public ColorPanel()
        {
            InitializeComponent();
        }

        public ColorPanel(XCommand cmd)
        {
            InitializeComponent();
            this.btnRed.Click += new RoutedEventHandler(cmd.aColor.Action);
            this.btnBlue.Click += new RoutedEventHandler(cmd.aColor.Action);
            this.btnGreen.Click += new RoutedEventHandler(cmd.aColor.Action);
        }
    }
}
