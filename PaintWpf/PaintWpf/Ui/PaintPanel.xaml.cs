﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaintWpf
{
    /// <summary>
    /// Interaction logic for PaintPanel.xaml
    /// </summary>
    public partial class PaintPanel : UserControl
    {
        public XData data = new XData();
        public XCommand cmd;
        private Point prev;
        public PaintPanel()
        {
            InitializeComponent();
        }

        public PaintPanel(XCommand cmd)
        {
            InitializeComponent();
            this.cmd = cmd;
            SelfRef = this;
        }

        public static PaintPanel SelfRef
        {
            get; set;
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            prev = Mouse.GetPosition(canvas);
        }

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var point = Mouse.GetPosition(canvas);
                var line = new Line
                {
                    Stroke = cmd.data.color,
                    StrokeThickness = cmd.data.width,
                    X1 = prev.X,
                    Y1 = prev.Y,
                    X2 = point.X,
                    Y2 = point.Y,
                    StrokeStartLineCap = PenLineCap.Round,
                    StrokeEndLineCap = PenLineCap.Round
                };
                canvas.Children.Add(line);
                prev = point;              
            }
        }

        public Canvas GetMemento()
        {
            return canvas;
        }
        public void SetMemento(Image image)
        {
            canvas.Children.Add(image);
        }

        private void canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            prev = new Point();
        }

        private void canvas_MouseEnter(object sender, MouseEventArgs e)
        {
            prev = Mouse.GetPosition(canvas);
        }
    }
}
