﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PaintWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        XData data;
        public XCommand cmd = new XCommand();
        ColorPanel cp;
        PaintPanel pp;
        WidthPanel wp;
        SaveLoadPanel slp;
        public MainWindow()
        {
            data = new XData();
            cmd.data = data;
            cp = new ColorPanel(cmd);
            pp = new PaintPanel(cmd);
            wp = new WidthPanel(cmd);
            slp = new SaveLoadPanel(cmd);
            InitializeComponent();
            DefinedControls();
            cmd.pp = PaintPanel.SelfRef;
        }

        private void DefinedControls()
        {
            wp.Margin = new Thickness(10, 0, 0, 0);
            wp.Height = 200;
            wp.Width = 200;
            wp.HorizontalAlignment = HorizontalAlignment.Left;
            wp.VerticalAlignment = VerticalAlignment.Top;
            grid.Children.Add(wp);

            cp.Margin = new Thickness(10, 150, 0, 0);
            cp.Height = 200;
            cp.Width = 200;
            cp.HorizontalAlignment = HorizontalAlignment.Left;
            cp.VerticalAlignment = VerticalAlignment.Top;
            grid.Children.Add(cp);

            slp.Margin = new Thickness(35, 290, 0, 0);
            slp.Height = 200;
            slp.Width = 200;
            slp.HorizontalAlignment = HorizontalAlignment.Left;
            slp.VerticalAlignment = VerticalAlignment.Top;
            grid.Children.Add(slp);

            pp.Margin = new Thickness(178, 0, 0, 0);
            pp.Height = 340;
            pp.Width = 500;
            grid.Children.Add(pp);           
        }

    }
}
