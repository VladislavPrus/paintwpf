﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace PaintWpf
{
    public class XData
    {
        public SolidColorBrush color;
        public int width;

        public XData()
        {
            color = new SolidColorBrush(Colors.Black);
            width = 5;
        }
    }
}
