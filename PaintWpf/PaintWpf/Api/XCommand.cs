﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PaintWpf
{
    public class XCommand
    {
        public abstract class ICmd
        {
            public XCommand cmd = null;
            public abstract void Action(object sender, EventArgs e);
        }

        public PaintPanel pp = null;

        public ActionColor aColor;
        public ActionWidth aWidth;
        public ActionSave aSave;
        public ActionLoad aLoad;
        public XData d;

        public XCommand()
        {
            aColor = new ActionColor(this);
            aWidth = new ActionWidth(this);
            aSave = new ActionSave(this);
            aLoad = new ActionLoad(this);
        }

        public XData data
        {
            get
            {
                return d;
            }
            set
            {
                d = value;
            }
        }

        public class ActionWidth : ICmd
        {
            public ActionWidth(XCommand cmd)
            {
                this.cmd = cmd;
            }
            public override void Action(object sender, EventArgs e)
            {
                string s = "";
                s = ((Control)sender).Tag.ToString();
                cmd.pp.data.width = Convert.ToInt32(s);
                cmd.data = cmd.pp.data;
            }
        }
        public class ActionColor : ICmd
        {
            public ActionColor(XCommand cmd)
            {
                this.cmd = cmd;
            }
            public override void Action(object sender, EventArgs e)
            {
                switch (((Control)sender).Name)
                {
                    case "btnRed":
                        cmd.pp.data.color = new SolidColorBrush(Colors.Red);
                        break;
                    case "btnBlue":
                        cmd.pp.data.color = new SolidColorBrush(Colors.Blue);
                        break;
                    case "btnGreen":
                        cmd.pp.data.color = new SolidColorBrush(Colors.Green);
                        break;
                }
                cmd.data = cmd.pp.data;
            }
        }
        public class ActionSave : ICmd
        {
            public ActionSave(XCommand cmd)
            {
                this.cmd = cmd;
            }
            public override void Action(object sender, EventArgs e)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.InitialDirectory = @"E:\";
                sfd.Filter = "bmp(.bmp)|*.bmp";
                if (sfd.ShowDialog() == true)
                {
                    PictureBmp pb = new PictureBmp();
                    pb.Save(sfd.FileName,cmd.pp.GetMemento());
                }
            }
        }
        public class ActionLoad : ICmd
        {
            public ActionLoad(XCommand cmd)
            {
                this.cmd = cmd;
            }
            public override void Action(object sender, EventArgs e)
            {
                FileDialog fd = new OpenFileDialog();
                fd.InitialDirectory = @"E:\";
                fd.Filter = "bmp(.bmp)|*.bmp";
                if (fd.ShowDialog() == true)
                {
                    PictureBmp pb = new PictureBmp();
                    cmd.pp.SetMemento(pb.Load(fd.FileName));
                }
            }
        }

    }
}
